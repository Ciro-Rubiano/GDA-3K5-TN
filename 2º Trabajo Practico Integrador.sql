
--Ejercicio 1

--a

create table premioXLibro
    (codLibro varchar(15),
    idGenero varchar(10),
    añoOtorgamiento varchar(4) default year(getDate()),
    codPremio varchar(10) not null,
    constraint premioXLibro_PK primary key(codLibro, idGenero, añoOtorgamiento),
    constraint premioXLibro_libro_genero_FK foreign key(codLibro, idGenero)
        references ebook(codigo, idGenero),
    constraint premioXLibro_codPremio_FK foreign key(codPremio)
        references Premio(codPremio))


--b

insert into ebook
values(1234, 15, "Farenheit 451", "Quemar libros", 2016, 20, 438)

insert into ebook
values(3212, 15, "Un mundo feliz", "Control farmacológico", 2013, 21, 765)

insert into ebook
values(954, 15, "1984", "Gran hermano", 2018, null, null)

--c

update descuentos
    set montoSuperior = montoSuperior + montoSuperior*0.20
    where montoInf <= 1000

--si quisiera que se aplique solo a una fila

update descuentos 
    set montoSuperior = montoSuperior + montoSuperio*0.20
    where montoInf <= 1000
    and montoInf = n
    and fechadesde = CONVERT (datetime,'dd/mm/YYYY',103)
/* En el momento de ejecucion deben completarse las variables n y dd/mm/YYYY con la primary key de la fila que desamos modificar 
*/

--d

delete from tarea
where nombre = "Re-entrega"

/*
 La eliminación de una fila en la tabla referenciada no provocará la 
 eliminación de las filas que se encuentren en tablas que contienen
 las referencias y que posean valores coindicentes con los registros
 eliminados.
 
 El problema de que la BD este configurada de este modo es que puede
 generar problemas de integridad referencial al tener referencias a
 registros inexistentes, se poner una restriccion (RESTRICT / NO ACTION)
 para evitar la eliminación de registros referenciados o configurar la
 db como "On Delete Cascade ON".
*/

--e

select distinct E.titulo
from ebook E
where (year(getDate()) - E.añoEdicion) >= 10
and E.titulo in (select L.titulo
                 from Ebook L
                 where E.titulo = L.titulo
                 and E.numeroEdicion <> L.numeroEdicion)

--e opcion 2
select E.titulo
from ebook E
where (year(getDate()) - E.añoEdicion) >= 10
group by E.titulo
having COUNT(*) > 1

--f

select A.apellido, A.nombre, SUM(C.cantPaginas)
from autorCliente A, autorXCap AC, composicion C
where A.nroDocumento = AC.nroDoc
and A.tipoDocumento = AC.tipoDoc
and C.codLibro = AC.codLibro
and C.idGenero = AC.idGenero
and C.nroCapitulo = AC.nroCapitulo
and AC.idGenero = "Narrativa"
group by A.nroDocumento, A.nroDocumento, A.apellido, A.nombre
having SUM(C.cantPaginas) > 1000
order by A.apellido, A.nombre

--f opcion 2

select A.apellido, A.nombre
from autorCliente A join autorXCap AC on (A.nroDocumento = AC.nroDoc and A.tipoDocumento = AC.tipoDoc)
join composicion C on (AC.codLibro = C.codLibro and AC.idGenero = C.idGenero and AC.nroCapitulo = C.nroCapitulo)
where  AC.idGenero = 'Narrativa'
group by A.tipoDocumento, A.nroDocumento, A.Apellido, A.Nombre
having SUM(C.cantPaginas) > 1000
order by A.apellido, A.nombre

--g

select P.nombre, I.nombre
from premio P, premioXLibro PL, ebook E, institucion I
where P.codPremio = PL.codPremio
and PL.codLibro = E.codigo
and PL.idGenero = E.idGenero
and P.idInstitucion = I.idInstitucion
and I.idInstitucion between 200 and 350
and PL.añoOtorgamiento = year(getDate())
and E.titulo like '%ciencia%'


--h 

select S.razonSocial as "Razon Social", S.añoInicioVentas
from sitio S, venta V, detalleVenta DV
where S.cuil = DV.cuilSitio
and DV.nroFactura = V.nroFactura
and year(V.fecha) = year(getDate())
group by S.razonSocial, S.añoInicioVentas, S.cuil
having SUM(DV.cantidad) > (select SUM(DVE.cantidad)
                           from venta VE, detalleVenta DVE
                           where DVE.cuilSitio = S.cuil
                           and DVE.nroFactura = VE.factura
                           and year(VE.fecha) = (year(getDate()) - 1))
order by "Razon Social"
                      
--h opcion 2
select S.razonSocial, S.añoInicioVentas
from sitio S join venta V on (S.cuil = V.cuil) join detalleVenta DV on (V.nroFactura = DV.nroFactura)
where YEAR(V.fecha) = YEAR(getDate())
group by S.cuil, S.razonSocial, S.añoInicioVentas
having SUM(DV.cantidad) > (select SUM(DVV.cantidad)
                            from venta VT join detalleVenta DVV on (VT.nroFactura = DVV.nroFactura)
                            where YEAR(VTT.fecha) = (YEAR(getDate()) - 1)
                            and VT.cuilSitio = S.cuil
                            )
order by S.razonSocial

--i

select EM.legajo "Legajo", EM.nombre
from empleadoXVenta EV, empleado EM, venta V
where EV.legajo = EM.legajo
and EV.nroFactura = V.nroFactura
and year(V.fecha) = (year(getDate()) - 1)
and month(V.fecha) between 01 and 06
group by EV.cuilSitio, EM.legajo, EM.nombre
having count(*) >= ALL (select count(*)
                        from empleadoXVenta EXV, venta VE
                        and EXV.legajo = EM.legajo
                        and EXV.nroFactura = VE.nroFactura
                        and year(VE.fecha) = (year(getDate()) - 1)
                        and month(VE.fecha) between 01 and 06
                        and EXV.cuilSitio = EV.cuilSitio)
order by "Legajo" desc

--j lucas

SELECT G.nombre, SB.nombre
FROM subGeneroXLibro SL 
JOIN idSubgenero SB on (SL.idSubgenero = SB.idSubgenero) 
JOIN genero G on (SL.idGenero = G.idGenero)
GROUP by SL.codLibro, SL.idGenero, G.nombre, SB.nombre
HAVING count(*) > 1
ORDER BY G.nombre, SB.nombre

--k

select E.codigo,E.idGero, E.titulo
from ebookHisto EH, ebook E
where EH.codigo = E.codigo
and EH.idGenero = E.idGenero
and year(EH.fecha_desde) = (year(getDate()) - 1)
group by E.codigo, E.idGenero, E.titulo
having count(*) > 3

--l

select A.nroDocumento as "Nro Documento", TD.nombre as "Tipo Documento", A.fechaNac as "Fecha Nacimiento", A.nombre as "Nombre", A.apellido as "Apellido", A.mail as "Mail"
from autorXCap AC, ebook E, autorCliente A, tipoDocumento TD
where AC.codLibro = E.codigo
and AC.idGenero = E.idGenero
and AC.nroDoc = A.nroDocumento
and AC.tipoDoc = A.tipoDocumento
and A.tipoDocumento = TD.idTipoDoc
and year(E.añoEdicion) = year(getDate())
and E.titulo not in (select EB.titulo
                     from ebook EB
                     where AC.codLibro = EB.codigo
                     and AC.idGenero = EB.codigo
                     and year(EB.añoEdicion) = (year(getDate()) - 1))
group by A.nroDocumento, TD.nombre, A.fechaNac, A.nombre, A.apellido, A.mail

--ejercicio 2

--m

SELECT G.nombre as "Nombre Género", SB.nombre as "Nombre Subgénero", count(*) as "Count(*)"
FROM subGeneroXLibro SL 
JOIN idSubgenero SB on (SL.idSubgenero = SB.idSubgenero) 
JOIN genero G on (SL.idGenero = G.idGenero)
GROUP by G.nombre, SB.nombre
order by G.nombre desc

--n

-- se puede agrupar por libro para contar cuantos subgeneros tiene

-- se puede agrupar por subgenero para contar en cuantos generos está presente


-- Ejercio 3

--o

--i
/*Debe tener 3 joins(cantidad de tablas - 1).
Si la cantidad de emparejamientos (joins) no es suficiente, es decir que es menor a la cantidad de tablas - 1, se hace un producto cartesiano provocando que la cantidad de filas de resultado será un múltiplo de la cantidad de filas de la/s tabla/s que no estén emparejadas


por ejemplo en el enunciado "F opcion 2", si faltase el 2do Join provocaria que por cada entrada de la tabla autor se tenga n cantidad de combinaciones de filas. Siendo n la cantidad de Composiciones registradas en la tabla Composicion
*/
-- otro ejemplo:
--mostrar los libros cuyo género sea Narrativa y nombre de editorial contenga la palabra "universidad"

--faltaria el join de ebook con editorial, esto provoca un producto cartesiano
select E.codigo "Codigo Libro",E.titulo "Titulo", ED.nombre "Nombre editorial"
from ebook E, genero G, editorial ED
where E.idGenero = G.idGenero
and G.idGenero = "Narrativa"
and ED.nombre like "%universidad%"

--ii
--4. Listaría un producto cartesiano de las tablas declaradas en la cláusula FROM, en este caso serían 7500 filas.

--iii
--1. Da error porque en el select quiere mostrar S.razonSocial cuando no se encuentra en el group by

--Mostrar la razón social de los sitios con más de 5 sucursales en la provincia de Córdoba

SELECT S.razonSocial, count(*)
FROM Sitio S, Barrio B, Localidad L, Provincia P
WHERE S.barrio = B.idBarrio
    AND B.idLocalidad = L.idLocalidad
    AND L.idProvincia = P.idProvincia
    AND P.nombre = "Cordoba"
GROUP BY S.razonSocial
HAVING count(*) > 5

--iv

SELECT C.apellido, C.nombre
FROM AutorClientes C
WHERE NOT EXISTS (SELECT 1
                 FROM Ventas V
                 WHERE V.tipoDocCliente = C.tipoDocumento
                 AND YEAR(V.fecha)=(YEAR(GETDATE())-1)
                 AND C.nroDocumento = V.nroDocCliente)
                             
AND EXISTS (SELECT 1
           FROM Ventas V
           WHERE V.tipodocCliente= C.tipoDocumento
           AND YEAR(V.fecha)=YEAR(GETDATE())
           C.nroDocumento = V.nrodocCliente)

/*   
1. FROM busca la tabla "AutorClientes"
 
2. WHERE donde la condicion de "NO EXISTS" se contrasta con la subconsulta.
 
3. Resuelve la 1era subconsulta:
    3.a FROM busca la tabla "Ventas"
    3.b WHERE donde verifica que el tipo de documento del cliente de la venta con Una referencia externa al tipo de documento de la tabla "AutoresClientes" 
    y de la misma manera para el nro de documento del cliente de la venta con el nro de documento que hace referencia externa a la tabla "AutoresClientes" 
    y el año de la venta sea igual al año pasado
    3.c SELECT devuelve 1 si  la condicion del where es verdadera

    Unidos por un "AND"

4. Resuelve la 2da subconsulta(Contra EXISTS):
    4.a FROM busca la tabla "Ventas"
    4.b WHERE donde verifica que el tipo de documento del cliente de la venta con Una referencia externa al tipo de documento de la tabla "AutoresClientes" 
    y de la misma manera para el nro de documento del cliente de la venta con el nro de documento que hace referencia externa a la tabla "AutoresClientes" 
    y el año de la venta sea igual al año actual
    4.c SELECT devuelve 1 si  la condicion del where es verdadera

5. SELECT selecciona del resultado obtenido del WHERE las columnas "C.apellido" y "C.nombre".
*/

--v
--Mostrar el apellido y nombre de los clientes que hayan comprado este año y no el pasado