--a
create table DetallePedido
(
    renglon INT,
    idPedido INT
    cantidad decimal(10,3)
    precioUnitario decimal(10,3)
    idMaterial INT
    idProveedor INT
    
    constraint renglon_detallePedido_PF primary key (idPedido, renglon)
    constraint idMaterialIdProveedor_FK foreign key (idMaterial, idProveedor) references Materiales(idMAterial, idProveedor)
    constraint idPedido_FK foreign key Pedidos(idPedido)
)

--b

insert into Materiales
VALUES (236, 256, 'Hierro', 456.23, 6541.36, 24)


--c

update Materiales
set PrecioLista = PrecioLista*1.05
where stock < puntoRepocicion*1.2
and stock > puntoRepocicion

--2

select 
from DetallePedido DP join Pedido P on (DP.idPedido = P.id) join Cliente C on (P.idCliente = C.idCliente) join Materiales M on(DP.idMaterial = M.idMaterial and DP.idProveedor = M.idProveedor) join Proveedores PR on (PR.idProveedor = M.idProveedor)
where PR.Provincia in ('Cordoba', 'Santa Fe', 'Entre Rios')
and YEAR(P.fechaPedido) = 2007
order by P.fecha, C.Apellido, C.nombre
